class CreateTask < ActiveRecord::Migration
  def up
    create_table :tasks do |t|
      t.string :title, null: false, index: true
      t.text :description
      t.references :board, null: false, index: true
      t.datetime :completed_at, index: true

      t.timestamps null: false
    end
  end

  def down
    add_index :boards, :title
    add_index :boards, :board
    add_index :boards, :completed_at
    drop_table :tasks
  end
end
