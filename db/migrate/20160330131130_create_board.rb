class CreateBoard < ActiveRecord::Migration
  def up
    create_table :boards do |t|
      t.string :title, null: false, index: true
      t.text :description

      t.timestamps null: false
    end
  end

  def down
    remove_index :boards, :title
    drop_table :boards
  end
end
