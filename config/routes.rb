Rails.application.routes.draw do
  root 'dashboard#index'

  resources :boards, except: [:new, :edit]
  resources :tasks, except: [:new, :edit] do
    member do
      put :complete
    end
  end
end
