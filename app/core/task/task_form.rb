class Task::TaskForm
  include Virtus.model(:strict => true)
  include ActiveModel::Validations

  attribute :title, Common::Attr::StrippedString
  attribute :description, Common::Attr::StrippedString
  attribute :board_id, Integer

  validates :title, presence: true, length: { maximum: 140 }
  validates :description, length: { maximum: 1000 }, allow_blank: true
  validates :board_id, presence: true, numericality: { only_integer: true }

  def valid?
    super && custom_valid?
  end

  private

  def custom_valid?
    !(board_dont_exist? || title_in_use?)
  end

  def board_dont_exist?
    errors.add(:board, 'doesn\'t exist') unless Board.exists? board_id
  end

  def title_in_use?
    errors.add(:title, 'is already in use') if Task.find_by title: title, board_id: board_id
  end
end
