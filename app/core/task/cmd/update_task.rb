class Task::Cmd::UpdateTask < Task::BaseTaskCommand
  attr_reader :task, :form, :err_msg

  def initialize(params = {})
    @task = Task.find_by id: params[:id]
    @form = Task::TaskForm.new(params)
  end

  def call
    return broadcast(:not_found) unless task
    return broadcast(:invalid, form.errors) unless form.valid?
    update_task!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok, task_hash)
  end

  private

  def update_task!
    @task = Task.create form.attributes
  rescue => e
    @err_msg = "Updating task: #{e.message}"
  end
end
