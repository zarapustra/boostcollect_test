class Task::Cmd::ShowTask < Task::BaseTaskCommand
  attr_reader :task
  def initialize(id)
    @task = Task.find_by(id: id)
  end

  def call
    return broadcast(:not_found) unless task
    broadcast(:ok, task_hash)
  end
end
