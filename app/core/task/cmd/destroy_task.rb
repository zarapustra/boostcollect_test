class Task::Cmd::DestroyTask < Rectify::Command
  attr_reader :task, :err_msg

  def initialize(id)
    @task = Task.find_by(id: id)
  end

  def call
    return broadcast(:not_found) unless task
    destroy_task!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok)
  end

  private

  def destroy_task!
    task.destroy
  rescue => e
    @err_msg = "Destroying task: #{e.message}"
  end
end
