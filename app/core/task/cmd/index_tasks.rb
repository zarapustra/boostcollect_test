class Task::Cmd::IndexTasks < Rectify::Command
  COMPLETE_CONDITIONS = {
    completed:   "completed_at is not null",
    incompleted: "completed_at is null"
  }

  def initialize(params = {})
    complete_condition = COMPLETE_CONDITIONS[params[:type]&.to_sym]
    @collection        = Task.where("board_id =?", params[:board_id]) # sanitizing params
                           .where(complete_condition)
  end

  def call
    broadcast(:ok, tasks_hash)
  end

  private

  def tasks_hash
    { tasks: tasks_payload }
  end

  def tasks_payload
    @collection.map do |task|
      Task::TaskPresenter.new(task: task).to_hash
    end
  end
end
