class Task::Cmd::CompleteTask < Task::BaseTaskCommand
  attr_reader :task, :err_msg

  def initialize(id)
    @task = Task.find_by id: id
  end

  def call
    return broadcast(:not_found) unless task
    complete_task!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok, task_hash)
  end

  private

  def complete_task!
    task.update(completed_at: Time.now) # only server time is saved in db
  rescue => e
    @err_msg = "Completing task: #{e.message}"
  end
end
