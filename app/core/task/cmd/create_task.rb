class Task::Cmd::CreateTask < Task::BaseTaskCommand
  attr_reader :form, :task, :err_msg

  def initialize(params = {})
    @form = Task::TaskForm.new(params)
  end

  def call
    return broadcast(:invalid, form.errors) unless form.valid?
    create_task!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok, task_hash)
  end

  private

  def create_task!
    @task = Task.create form.attributes
  rescue => e
    @err_msg = "Creating task: #{e.message}"
  end
end
