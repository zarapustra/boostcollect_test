class Task::TaskPresenter < Rectify::Presenter
  attribute :task, Task

  def to_hash
    {
      id:          task.id,
      title:       task.title,
      description: task.description
    }
  end
end
