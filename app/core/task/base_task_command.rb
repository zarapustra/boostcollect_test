class Task::BaseTaskCommand < Rectify::Command
  private

  def task_hash
    { task: Task::TaskPresenter.new(task: task).to_hash }
  end
end