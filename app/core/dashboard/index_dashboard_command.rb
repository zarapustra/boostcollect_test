class Dashboard::IndexDashboardCommand < Rectify::Command

  def initialize
    @total_boards           = Board.all.count
    @total_tasks            = Task.all.count
    @total_incomplete_tasks = Task.where(completed_at: nil).count
  end

  def call
    broadcast(:ok, dashboard_hash)
  end

  private

  def dashboard_hash
    { total_boards:           @total_boards,
      total_tasks:            @total_tasks,
      total_incomplete_tasks: @total_incomplete_tasks
    }
  end
end
