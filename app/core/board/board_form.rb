class Board::BoardForm
  include Virtus.model(:strict => true)
  include ActiveModel::Validations

  attribute :title, Common::Attr::StrippedString
  attribute :description, Common::Attr::StrippedString

  validates :title, presence: true, length: { maximum: 80 }
  validates :description, length: { maximum: 1000 }, allow_blank: true

  def valid?
    super && custom_valid?
  end

  private

  def custom_valid?
    !title_in_use?
  end

  def title_in_use?
    errors.add(:title, 'is already in use') if Board.find_by title: title
  end
end
