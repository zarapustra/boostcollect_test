class Board::BoardPresenter < Rectify::Presenter
  attribute :board, Board

  def to_hash
    {
      id:          board.id,
      title:       board.title,
      description: board.description
    }
  end
end
