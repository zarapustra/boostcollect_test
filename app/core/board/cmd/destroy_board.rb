class Board::Cmd::DestroyBoard < Rectify::Command
  attr_accessor :board, :err_msg

  def initialize(id)
    @board = Board.find_by(id: id)
  end

  def call
    return broadcast(:not_found) unless board
    destroy_board!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok)
  end

  private

  def destroy_board!
    board.destroy
  rescue => e
    @err_msg = "Destroying board: #{e.message}"
  end
end
