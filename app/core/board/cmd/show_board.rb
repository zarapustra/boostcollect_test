class Board::Cmd::ShowBoard < Rectify::Command

  def initialize(id)
    @board = Board.find_by(id: id)
  end

  def call
    return broadcast(:not_found) unless @board
    broadcast(:ok, board_hash)
  end

  private

  def board_hash
    { board: Board::BoardPresenter.new(board: @board).to_hash }
  end
end
