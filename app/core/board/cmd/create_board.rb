class Board::Cmd::CreateBoard < Rectify::Command
  attr_reader :form
  attr_accessor :board, :err_msg

  def initialize(params = {})
    @form = Board::BoardForm.new(params)
  end

  def call
    return broadcast(:invalid, form.errors) unless form.valid?
    create_board!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok, board_hash)
  end

  private

  def create_board!
    @board = Board.create form.attributes
  rescue => e
    @err_msg = "Creating board: #{e.message}"
  end

  def board_hash
    { board: Board::BoardPresenter.new(board: board).to_hash }
  end
end
