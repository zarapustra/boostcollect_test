class Board::Cmd::IndexBoards < Rectify::Command

  def initialize
    @collection = Board.all
  end

  def call
    broadcast(:ok, boards_hash)
  end

  private

  def boards_hash
    { boards: boards_payload }
  end

  def boards_payload
    @collection.map do |resource|
      Board::BoardPresenter.new(board: resource).to_hash
    end
  end
end
