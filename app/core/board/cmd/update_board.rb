class Board::Cmd::UpdateBoard < Rectify::Command
  attr_reader :form, :board, :err_msg

  def initialize(params = {})
    @board = Board.find_by id: params[:id]
    @form  = Board::BoardForm.new(params)
  end

  def call
    return broadcast(:not_found) unless board
    return broadcast(:invalid, form.errors) unless form.valid?
    update_board!
    return broadcast(:server_error, err_msg) if err_msg
    # ... callbacks
    broadcast(:ok, board_hash)
  end

  private

  def update_board!
    board.update form.attributes
  rescue => e
    @err_msg = "Creating board: #{e.message}"
  end

  def board_hash
    { board: Board::BoardPresenter.new(board: board).to_hash }
  end
end
