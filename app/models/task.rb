class Task < ActiveRecord::Base
  belongs_to :board, inverse_of: :tasks
end
