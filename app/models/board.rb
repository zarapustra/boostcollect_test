class Board < ActiveRecord::Base
  has_many :tasks, inverse_of: :board, dependent: :destroy
end
