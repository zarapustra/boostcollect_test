class DashboardController < ApplicationController
  def index
    cmd = Dashboard::IndexDashboardCommand.new
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end
end
