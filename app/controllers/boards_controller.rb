class BoardsController < ApplicationController
  def index
    cmd = Board::Cmd::IndexBoards.new
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def show
    cmd = Board::Cmd::ShowBoard.new params[:id]
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def create
    cmd = Board::Cmd::CreateBoard.new params
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def update
    cmd = Board::Cmd::UpdateBoard.new params
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def destroy
    cmd = Board::Cmd::DestroyBoard.new params[:id]
    err_handler(cmd).on(:ok) { render nothing: true, status: 204 }.call
  end
end
