class TasksController < ApplicationController

  def index
    cmd = Task::Cmd::IndexTasks.new params
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def show
    cmd = Task::Cmd::ShowTask.new params[:id]
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def create
    cmd = Task::Cmd::CreateTask.new params
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def complete
    cmd = Task::Cmd::CompleteTask.new params[:id]
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def update
    cmd = Task::Cmd::UpdateTask.new params
    err_handler(cmd).on(:ok) { |hash| render json: hash }.call
  end

  def destroy
    cmd = Task::Cmd::DestroyTask.new params[:id]
    err_handler(cmd).on(:ok) { head :no_content }.call
  end
end

=begin

# если всегда выводить json,
# подправить некоторые детали в командах
# и придерживаться этой конвенции,
# то можно сократить всё до подобной конструкции

%w(index show create complete update destroy).map do |action_name|
  define_method(action_name) do
    cmd_class = "Task::Cmd::#{action_name.capitalize.constantize}Task"
    cmd       = cmd_class.public_send(:new, params)
    err_handler(cmd).on(:ok) { |json| render json: json }.call
  end
end

=end
