class ApplicationController < ActionController::Base

  # можно сделать версионирование апи

  def err_handler(cmd)
    cmd.on(:not_found) {
      render nothing: true, status: 404
    }
    cmd.on(:invalid) { |errors|
      render status: 422, json: { errors: errors }
    }
    cmd.on(:server_error) do |msg|
      logger.error(msg)
      render status: 500
    end
  end
end
