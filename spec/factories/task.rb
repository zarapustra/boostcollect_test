FactoryBot.define do
  factory :task do
    title { Faker::TheITCrowd.actor }
    description { Faker::TheITCrowd.quote }
    board { create :board }

    factory :completed_task do
      after :create do |task|
        Task::Cmd::CompleteTask.new(task.id).call
      end
    end
  end
end
