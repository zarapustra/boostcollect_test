FactoryBot.define do
  factory :board do
    title { Faker::TheITCrowd.actor }
    description { Faker::TheITCrowd.quote }
  end
end
