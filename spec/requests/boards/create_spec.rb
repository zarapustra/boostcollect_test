require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  CREATE BOARD  -------------------->

describe 'Creating a board' do
  let(:title) { 'Title' }
  let(:description) { 'Description' }
  let(:params) do
    { title: title, description: description }
  end
  let(:board) { Board.last }
  before { post '/boards', params }

  context 'when all params are valid' do
    it 'creates a board' do
      expect(Board.count).to eq(1)
    end

    it_behaves_like 'it renders record\'s json', 'board'
  end

  include_context 'title'
  include_context 'description'

  context 'when TITLE is too long' do
    let(:title) { 'a' * 81 }
    it_behaves_like 'invalid', :title, 'is too long (maximum is 80 characters)'
  end

  context 'when title in use' do
    let!(:req) { post "/boards", params }
    it_behaves_like 'invalid', :title, 'is already in use'
  end
end
