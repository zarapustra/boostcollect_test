require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  UPDATE BOARD  -------------------->

describe 'Updating a board' do
  let(:title) { 'Title' }
  let(:description) { 'Description' }
  let(:params) do
    { title: title, description: description }
  end
  let(:board) { create :board }
  let(:id) { board.id }
  let(:other_board) { create :board }
  before { put "/boards/#{id}", params }

  context 'when all params are valid' do
    it_behaves_like 'it renders record\'s json', 'board'
  end

  include_context 'title'
  include_context 'description'
  include_context 'not found'

  context 'when TITLE is too long' do
    let(:title) { 'a' * 81 }
    it_behaves_like 'invalid', :title, 'is too long (maximum is 80 characters)'
  end

  context 'when title in use' do
    let!(:req) { put "/boards/#{id}", params.merge(title: other_board.title) }
    it_behaves_like 'invalid', :title, 'is already in use'
  end
end
