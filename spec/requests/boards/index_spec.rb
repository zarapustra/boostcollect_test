require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  INDEX BOARDS  -------------------->

describe 'Indexing boards' do
  let!(:boards) do
    3.times.collect { create :board }
  end
  before { get '/boards' }

  context do
    it 'renders boards json' do
      expect(last_response).to match_response_schema('boards')
    end
  end
end
