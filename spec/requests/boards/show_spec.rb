require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  SHOW BOARD  -------------------->

describe 'Showing a board' do
  let(:board) { create :board }
  let(:id) { board.id }
  before { get "/boards/#{id}" }

  include_context 'not found'

  context 'when the record is existent' do
    it_behaves_like 'it renders record\'s json', 'board'
  end
end