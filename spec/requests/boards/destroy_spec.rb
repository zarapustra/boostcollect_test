require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  DESTROY BOARD  -------------------->

describe 'Destroying the board' do
  let(:board) { create :board }
  let(:tasks) { create_list :task, 3, board: board }
  let(:id) { board.id }
  before { delete "/boards/#{id}" }

  include_context 'not found'

  context 'that existed' do
    it 'actually destroys it' do
      expect(Board.exists? id).to eq(false)
    end
    it 'destroys associated tasks' do
      expect(Task.count).to eq(0)
    end
    it_behaves_like 'it responds with', 204
  end
end
