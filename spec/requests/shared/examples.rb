shared_examples 'invalid' do |sym, msg|
  it 'with code 422' do
    expect(last_response.status).to eq(422)
    expect(json[:errors][sym]).to eq([msg])
  end
end

shared_examples 'renders nothing' do
  it { expect(last_response.body).to eq(' ') }
end

shared_examples 'it responds with' do |code|
  it "code #{code}" do
    expect(last_response.status).to eq(code)
  end
end

shared_examples 'it renders record\'s json' do |schema_name|
  it 'renders records\'s json' do
    expect(last_response).to match_response_schema(schema_name)
  end
end

