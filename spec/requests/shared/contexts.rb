shared_context 'title' do
  context 'is null' do
    let(:title) { nil }
    it_behaves_like 'invalid', :title, 'can\'t be blank'
  end

  context 'has spaces around' do
    let(:title) { '    title    ' }
    it 'strips them' do
      expect(json.first[1][:title]).to eq(title.strip)
    end
  end
end

shared_context 'description' do
  context 'is null' do
    let(:description) { nil }
    it_behaves_like 'it responds with', 200
  end

  context 'has spaces around' do
    let(:description) { '    description    ' }
    it 'strips them' do
      expect(json.first[1][:description]).to eq(description.strip)
    end
  end
end

shared_context 'not found' do
  context 'when the record is no-existent' do
    let(:id) { 10 }
    it_behaves_like 'it responds with', 404
  end
end