require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  INDEX DASHBOARD  -------------------->

describe 'Indexing dashboard' do
  let(:boards) { create_list :board, 2 }
  let(:result_hash) {
    {
      total_boards:           2,
      total_tasks:            3,
      total_incomplete_tasks: 1
    }
  }
  before do
    create :completed_task, board: boards.first
    create :completed_task, board: boards.second
    create :task, board: boards.second
    get '/'
  end

  context do
    it 'renders valid json' do
      expect(last_response).to match_response_schema('dashboard')
    end

    it 'renders correct counters' do
      expect(json).to eq(result_hash)
    end
  end
end
