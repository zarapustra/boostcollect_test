require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  COMPLETE TASK  -------------------->

describe 'Completing a task' do
  let(:task) { create :task }
  let(:id) { task.id }
  before { put "/tasks/#{id}/complete" }

  context 'when task exists' do
    it_behaves_like 'it renders record\'s json', 'task'
  end
  context 'when task does not exist' do
    include_context 'not found'
  end
end
