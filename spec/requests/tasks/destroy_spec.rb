require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  DESTROY BOARD  -------------------->

describe 'Destroying a task' do
  let(:task) { create :task }
  let(:id) { task.id }
  before { delete "/tasks/#{id}" }

  include_context 'not found'

  context 'when trying to destroy existed record' do
    it 'destroys the record' do
      expect(Task.exists? id).to eq(false)
    end
    it_behaves_like 'it responds with', 204
  end
end