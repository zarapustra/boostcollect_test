require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  UPDATE TASK  -------------------->

def request!

end

describe 'Updating a task' do
  let(:title) { 'Title' }
  let(:description) { 'Description' }
  let(:task) { create :task }
  let(:id) { task.id }
  let(:board_id) { task.board_id }
  let(:other_task) { create :task, board_id: board_id }
  let(:params) do
    { title: title, description: description, board_id: board_id }
  end

  before { put "/tasks/#{id}", params }

  context 'when all params are valid' do
    it_behaves_like 'it renders record\'s json', 'task'
  end

  context 'when board doesn\'t exist' do
    let(:board_id) { 2 }
    it_behaves_like 'invalid', :board, 'doesn\'t exist'
  end
  include_context 'not found'
  include_context 'title'
  include_context 'description'

  context 'when TITLE is too long' do
    let(:title) { 'a' * 141 }
    it_behaves_like 'invalid', :title, 'is too long (maximum is 140 characters)'
  end

  context 'when title in use' do
    let!(:req) { put "/tasks/#{id}", params.merge(title: other_task.title) }
    it_behaves_like 'invalid', :title, 'is already in use'
  end
end
