require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  SHOW TASK  -------------------->

describe 'Showing a task' do
  let(:task) { create :task }
  let(:id) { task.id }
  before { get "/tasks/#{id}" }

  include_context 'not found'

  context 'when the record is existent' do
    it_behaves_like 'it renders record\'s json', 'task'
  end
end