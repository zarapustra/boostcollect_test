require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  CREATE TASK  -------------------->

describe 'Creating a task' do
  let(:title) { 'Title' }
  let(:description) { 'Description' }
  let(:board) { create :board }
  let(:board_id) { board.id }
  let(:params) do
    { title: title, description: description, board_id: board_id }
  end
  before { post "/tasks", params }

  context 'when all params are valid' do
    it 'creates a task' do
      expect(Task.count).to eq(1)
    end

    it_behaves_like 'it renders record\'s json', 'task'
  end

  context 'when board doesn\'t exist' do
    let(:board_id) { 2 }
    it_behaves_like 'invalid', :board, 'doesn\'t exist'
  end

  include_context 'title'
  include_context 'description'

  context 'when title is too long' do
    let(:title) { 'a' * 141 }
    it_behaves_like 'invalid', :title, 'is too long (maximum is 140 characters)'
  end

  context 'when title in use' do
    let!(:req) { post "/tasks", params }
    it_behaves_like 'invalid', :title, 'is already in use'
  end
end
