require 'rails_helper'
require Rails.root.join('spec/requests/shared/examples.rb')
require Rails.root.join('spec/requests/shared/contexts.rb')

#-------  INDEX TASKS  -------------------->

describe 'Indexing tasks' do
  let(:board) { create :board }
  let(:board_id) { board.id }
  let(:type) { nil }
  let(:params) do
    { board_id: board_id, type: type }
  end
  before do
    create_list :task, 2, board: board
    create :completed_task, board: board
    get "/tasks", params
  end

  context do
    it 'renders valid json' do
      expect(last_response).to match_response_schema('tasks')
    end

    it 'renders 3 tasks' do
      expect(json[:tasks].size).to eq(3)
    end
  end

  context 'when type is completed' do
    let(:type) { 'completed' }
    it 'renders 1 tasks' do
      expect(json[:tasks].size).to eq(1)
    end
  end
  context 'when type is completed' do
    let(:type) { 'incompleted' }
    it 'renders 2 tasks' do
      expect(json[:tasks].size).to eq(2)
    end
  end
end
