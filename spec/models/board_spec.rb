require 'rails_helper'

RSpec.describe Board, type: :model do
  let(:board) { create :board }

  context 'db' do
    context 'columns' do
      it { should have_db_column(:title).of_type(:string).with_options(null: false) }
      it { should have_db_column(:description).of_type(:text) }
      it { should have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
      it { should have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    end

    context 'indexes' do
      it { should have_db_index(:title) }
    end

    context 'associations' do
      it 'have many tasks' do
        expect(board).to have_many(:tasks)
      end
    end
  end
end