require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:task) { create :task }

  context 'db' do
    context 'columns' do
      it { should have_db_column(:title).of_type(:string).with_options(null: false) }
      it { should have_db_column(:description).of_type(:text) }
      it { should have_db_column(:board_id).of_type(:integer).with_options(null: false) }
      it { should have_db_column(:completed_at).of_type(:datetime) }
      it { should have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
      it { should have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    end

    context 'indexes' do
      it { should have_db_index(:title) }
      it { should have_db_index(:board_id) }
      it { should have_db_index(:completed_at) }
    end

    context 'associations' do
      it 'should belong_to' do
        expect(task).to belong_to(:board)
      end
    end
  end
end